<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package AutoDelen
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 's010288_zda');

/** MySQL database username */
define('DB_USER', 's010288_zda');

/** MySQL database password */
define('DB_PASSWORD', 'zda2013');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4vy-Xc`V+H,X`a0QpYz,Zu6y:?-N|k|Gq6Q!}#/wv}(#N )1&X9Ee0?+LU317I48');
define('SECURE_AUTH_KEY',  '`Qv@;D-*|{yzA+~K(%+-(8tmqnZ@|1<$rH+6pCZHT#9 c</j9+j63w% ^g+AX~R{');
define('LOGGED_IN_KEY',    ':pl=fvbF7+N [yPyH/~FNF$i-e1Aw,*a?pE_x)|n%aPD-/bDsOuHD;C:J|jzx+X,');
define('NONCE_KEY',        'kNPh^0LgT9&l6$ukP5 0J|$?9LF_l&/T]PmCb*arK.1?-SP4[Qp> .OdyP?&>v@s');
define('AUTH_SALT',        'J/<cArYf7.+,xqBM+hM$=:nzLn:2.LUfx#aeah^n!Q ~flp{C|$u2+Nx/BxA*Mi6');
define('SECURE_AUTH_SALT', 'NWbXe{`|r.U]X)B3=?uw(%?JD)RHYXZPo)O1P=L%lYe-SX6[6j|sW}U=O;YO)p}7');
define('LOGGED_IN_SALT',   '~:UXh@yRhP_E$M}HiZWd^!x;J]Z`]b#!}+dj[Ku~GJnMDNG(f-O?RvDnX0q|)F(&');
define('NONCE_SALT',       'WTYuz(+@wf-RqrG31,Ykaonk{EO}H<W;Iy&DudXanQY!S~6R.z|!yj+2pR+BUC%b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'nl_NL');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', TRUE);

/**
 * enable multisite
 */
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'deeltauto.local');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
