<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package AutoDelen
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

if (is_page('mijn-auto-delen')) {
    $bannerimg = '/images/icn-stap-in.png';
    $tiles_name = 'een';
    $tile_name = 'keen';
} elseif (is_page('deelnemer-worden')) {
    $bannerimg = '/images/icn-deelnemer-worden.png';
    $tiles_name = 'een';
    $tile_name = 'ktwee';
} elseif (is_page('auto-reserveren')) {
    $bannerimg = '/images/icn-auto-zoeken.png';
    $tiles_name = 'een';
    $tile_name = 'kdrie';
} elseif (is_page('prikboard')) {
    $bannerimg = '/images/icn-prikboard.png';
    $tiles_name = 'twee';
    $tile_name = 'keen';
} elseif (is_page('het-concept')) {
    $bannerimg = '/images/icn-concept.png';
    $tiles_name = 'twee';
    $tile_name = 'ktwee';
} elseif (is_page('referenties')) {
    $bannerimg = '/images/icn-referenties.png';
    $tiles_name = 'twee';
    $tile_name = 'kdrie';
} else {
    $bannerimg = '/images/icn-auto.png'; // just in case we are at an unclassified page, perhaps the home page
    $tiles_name = 'een';
    $tile_name = 'keen';
}
get_header();?>
                    <!-- from content-page.php -->
                    <div class="autodelen-page" <?php if(is_page('auto-reserveren')) echo 'style="width:1000px;"';?>>
                        <div class="<?php echo $tiles_name; ?>">
                            <div class="<?php echo $tile_name; ?>">
                                <div class="autodelen-title">
                                    <h4><?php the_title(); ?></h4>
                                    <img src="<?php bloginfo('stylesheet_directory');echo $bannerimg; ?>" alt="<?php the_title(); ?>">
                                </div><!-- autodelen-title -->
                                <?php 
                                    if(is_page('auto-reserveren'))
                                        echo '<div class="content" style="width:100%;margin:0px;padding:0px;"><iframe src="http://www.autodelenzwolle.nl/auto/login.php" width="100%" height="600px"></iframe>';
                                    else {
                                        echo '<div class="content">';
                                        the_content();
                                    }
                                ?>       
                                </div><!-- content -->
                            </div><!-- <?php echo $tile_name; ?> -->
                        </div><!-- <?php echo $tiles_name; ?> -->
                        <div style="clear:both;"></div>
                    </div><!-- autodelen-page -->
<?php get_footer(); ?>
