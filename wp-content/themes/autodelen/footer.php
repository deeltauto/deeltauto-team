<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package AutoDelen
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
                    <?php if (!( is_home() || is_front_page()))
                        echo "</div><!-- content -->\n</div><!-- primairy -->";
                    ?>
                </div><!-- autodelen-pagina -->
                <div style="clear:both;"></div>
            </div><!--autodelen-canvas-->
            <div id="autodelen-footer">
                <div id="autodelen-footer-tekst">
                    <p>&copy;2013 <a href="http://www.autodelenzwolle.nl" target='_blank'>AUTODELEN ZWOLLE</a> - <a href="algemene-voorwaarden">VOORWAARDEN</a> - KVK ############ - CONCEPT: <a href="http://www.autodelenzwolle.nl" target='_blank'>AUTODELEN ZWOLLE</a> - ONTWERP: <a href="http://www.plusjoost.nl" target='_blank'>+JOOST. BEELDADVIES</a> - REALISATIE: <a href="http://www.energieeenheid.nl" target='_blank'>ENERGIE EENHEID</a> - DANK AAN: <a href="http://www.natuurenmilieuoverijssel.nl/" target='_blank'>NATUUR EN MILIEU OVERIJSSEL</a> EN <a href="http://www.greenwish.nl" target='_blank'>GREENWISH</a></p>
                </div><!-- autodelen-footer-tekst -->
            </div><!-- autodelen-footer -->
		</div><!--autodelen-wrapper-->
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>   
        
     </body>
</html>