<?php
/**
 * Auto Delen functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package AutoDelen
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */


/* VO:
 * fanction adde to get content
 */
function autodelen_get_excerpt_by_id($post = 0) {
//	$my_more = "<!--more-->";
    $post = get_post( $post );
    if ( !empty( $post ) ) {
//        global $more; $more = 0;
//        $excerpt = $post->post_excerpt;
          $my_more = "<!--more-->";
          $content = $post->post_content;
          $excerpt = strstr($content, $my_more);
        
    }
    return $excerpt;
}

add_action( 'get_excerpt_by_id', 'autodelen_get_excerpt_by_id' );
?>