<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package AutoDelen
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header(); ?>
                <!--from front-page.php-->                
                <div class="tiles een">
                    <div class="tile keen">
                        <div class="tile-content">
                            <a href="mijn-auto-delen" class="tilehover">
                                <h4><?php echo get_the_title(6); ?></h4>
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/icn-stap-in.png" alt="Mijn auto delen">
                                <div class="tilecontent">
                                    <p>Je bent eigenaar van een auto en je wilt die gaan delen.<br/>Je wordt dan beheerder van de deel auto en bent automatisch deelnemer.<br/><br/><i>Klik hier en kijk hoe je je auto beschikbaar kunt stellen als deelauto.</i></p>
                                </div><!-- tilecontent -->
                                <div style="clear:both;"></div>
                            </a>
                        </div> <!-- tile-content -->
                    </div><!-- tile keen -->
                    <div class="tile ktwee">
                        <div class="tile-content">
                            <a href="deelnemer-worden" class="tilehover">
                                <h4><?php echo get_the_title(24); ?></h4>
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/icn-deelnemer-worden.png" alt="Deelnemer worden">
                                <div class="tilecontent">
                                    <p>Je wilt graag over een deelauto beschikken.<br/><br/><i>Klik hier en kijk hoe je deelnemer kunt worden.</i></p>
                                </div><!-- tilecontent -->
                                <div style="clear:both;"></div>
                            </a>
                        </div> <!-- tile-content -->
                    </div><!-- tile ktwee -->
                    <div class="tile kdrie">
                        <div class="tile-content">
                            <a href="auto-reserveren" class="tilehover">
                                <h4><?php echo get_the_title(30); ?></h4>
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/icn-auto-zoeken.png" alt="Auto reserveren">
                                <div class="tilecontent">
                                    <p>Als je deelnemer bent, kun je hier deelauto's zoeken en reserveren en het gebruik ervan administreren.<br/><br/><i>Klik hier om in te loggen op het deelnemers gedeelte van de website.</i></p>
                                </div><!-- tilecontent -->
                                <div style="clear:both;"></div>
                            </a>
                        </div> <!-- tile-content -->
                    </div><!-- tile kdrie -->
                    <div style="clear: both;"></div>
                </div> <!-- tiles een -->
                <div class="tiles twee">
                    <div class="tile keen">
                        <div class="tile-content">
                            <a href="prikboard" class="tilehover">
                                <h4><?php echo get_the_title(33); ?></h4>
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/icn-prikboard.png" alt="Prikboard">
                                <div class="tilecontent">
                                    <p>Hier vind je vraag en aanbod van autodelers met betrekking tot autodelen.<br/><br/><i>Klik hier om oproepen te plaatsen.</i></p>
                                </div><!-- tilecontent -->
                                <div style="clear:both;"></div>
                            </a>
                        </div> <!-- tile-content -->
                    </div><!-- tile keen -->
                    <div class="tile ktwee">
                        <div class="tile-content">
                            <a href="het-concept" class="tilehover">
                                <h4><?php echo get_the_title(36); ?></h4>
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/icn-concept.png" alt="Het concept">
                                <div class="tilecontent">
                                    <p>Hier wordt ons autodelen concept uitgelegd.<br/><br/><i>Klik hier en kijk hoe ons concept werkt.</i></p>
                                </div><!-- tilecontent -->
                                <div style="clear:both;"></div>
                            </a>
                        </div> <!-- tile-content -->
                    </div><!-- tile ktwee -->
                    <div class="tile kdrie">
                        <div class="tile-content">
                            <a href="referenties" class="tilehover">
                                <h4><?php echo get_the_title(39); ?></h4>
                                <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/icn-referenties.png" alt="Referenties">
                                <div class="tilecontent">
                                    <p>Hier vind je ervaringen van deelnemers en delers.<br/>Hier kun je kennis maken en in contact komen met andere autodelers en de organisaties achter deze website.<br/><br/><i>Klik hier om ervaringen te delen en die van anderen te lezen.</i></p>
                                </div><!-- tilecontent -->
                                <div style="clear:both;"></div>
                            </a>
                        </div> <!-- tile-content -->
                    </div><!-- tile kdrie -->
                    <div style="clear: both;"></div>
                </div> <!-- tiles twee -->
<?php get_footer(); ?>
